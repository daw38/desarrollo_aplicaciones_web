**Desarrollo de aplicaciones web**
**5AVP**
**Ada Nicole Espinoza Diaz**

- Práctica #1 - 02/09/2022 - Práctica de ejemplo 
Commit: 70e99df4 
Archivo: https://gitlab.com/daw38/desarrollo_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html 
- Práctica #2 - 09/09/2022 - Práctica JavaScript
Commit: 385a9e00
Archivo: https://gitlab.com/daw38/desarrollo_aplicaciones_web/-/blob/main/parcial1/practicaJavaScript.html
- Práctica #3 - 15/09/2022 - Practica web con bases de datos - Parte 1
Commit: 28cd51a7
- Práctica #4 - 19/09/2022 - Práctica web con base de datos - Vista de consulta de datos
Commit:35f475d9
- Práctica #5 - 22/09/2022 - Práctica web con base de datos - Vista de registro de datos 
Commit:8b9e49f6
- Práctica #6 - 26/09/2022 - Práctica web con base de datos - Mostrar registro de consulta de datos 
Commit:65c0d980
